import codecs
import os

text = [input(u"Podaj swoje imię i nazwisko:\n"), "Zalety ESB:"]
for i in range(3):
    text.append(str(i+1) + ". " + input(u"Podaj zaletę ESB\n"))
text.append("Wady ESB:")
for i in range(3):
    text.append(str(i+1) + ". " + input(u"Podaj wadę ESB\n"))
with codecs.open("zadanie.txt", "w+", "utf-8") as file:
    for line in text:
        file.write(line + "\n")
    print("Plik 'zadanie.txt' zapisany w folderze: ", os.getcwd())